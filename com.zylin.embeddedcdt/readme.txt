To build the plugin:

- Create three projects: com.zylin.cdt.feature, com.zylin.cdt.updatesite
and com.zylin.embeddedcdt.
- Modify sourcecode.
- Right click on com.zylin.cdt.updatesite/site.xml and use 
PDE->build site. The plugin is stored in plugins/ directory.
