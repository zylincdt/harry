package com.zylin.embeddedcdt.gui.buttons;

import com.zylin.embeddedcdt.gui.jtag.IScript;

public interface IFancyCombo
{


	/** Notification that the script changed 
	 * @param source The script that changed  
	 **/
	public abstract void scriptChangedEvent(IScript source);

}