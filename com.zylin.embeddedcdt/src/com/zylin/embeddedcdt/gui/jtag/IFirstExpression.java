package com.zylin.embeddedcdt.gui.jtag;

public interface IFirstExpression
{

	/** Creates the first expression */
	String createFirstEntry(String script, String newText);

}
