package com.zylin.embeddedcdt.launch.jtag;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;

import com.zylin.embeddedcdt.gui.jtag.ConfigJTAGTab;
import com.zylin.embeddedcdt.launch.cygwin.CygWinLaunch;

public class JTAGLaunch extends CygWinLaunch implements
		ILaunchConfigurationDelegate
{

	@Override
	protected void uploadFile(IProgressMonitor monitor,
			ILaunchConfiguration configuration) throws CoreException
	{
		ConfigJTAGTab.uploadFile(configuration);
	}


}
